from prometheus_client import Gauge, start_http_server

class PrometheusAoIExposer:
    def __init__(self):
        self.average_peak_paoi = Gauge('average_peak_paoi', 'Average Peak Penalty Age of Information')
        self.number_of_vehicles = Gauge('number_of_vehicles', 'Number of vehicles')
        start_http_server(8000)  # Start Prometheus HTTP server on port 8000

    def set_average_peak_paoi(self, value):
        self.average_peak_paoi.set(value)
    
    def set_number_of_vehicles(self, value):
        self.number_of_vehicles.set(value)
