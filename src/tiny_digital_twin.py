"""
Class that implements a tiny LDM tracking vehicles positions.
"""
from __future__ import annotations
import utm
import math
import geopy.distance
import time

class Position:
    """
    Position class to store latitude and longitude.
    
    Attributes
    ----------
    latitude : float
        Latitude of the position.
    longitude : float
        Longitude of the position.
    speed : float
        Speed of the vehicle (in m/s).
    acceleration : float
        Acceleration of the vehicle (in m/s^2).
    heading : float
        Heading of the vehicle (in degrees).
    """
    timestamp_in_millis: int
    latitude: float
    longitude: float
    speed: float
    acceleration: float
    heading: float
    
    def __init__(self,
                timestamp_in_millis: int,
                latitude: float,
                longitude: float,
                speed: float,
                acceleration: float,
                heading: float,
                ) -> None:
        self.timestamp_in_millis = timestamp_in_millis
        self.latitude = latitude
        self.longitude = longitude
        self.speed = speed
        self.acceleration = acceleration
        self.heading = heading
    
    def distance(self, other: 'Position') -> float:
        """
        Calculate the distance between two positions.
        """
        coords_1 = (self.latitude, self.longitude)
        coords_2 = (other.latitude, other.longitude)
        return geopy.distance.geodesic(coords_1, coords_2).m

    def transform_to_utm(self) -> tuple:
        """
        Transform latitude and longitude to UTM.
        """
        return utm.from_latlon(self.latitude, self.longitude)

class PredictedPosition(Position):
    """
    Position predicted based on a real position. Extends Position class.
    
    Attributes
    ----------
    real_position : Position
        Real position used to predict the position.
    """
    real_position: Position
    
    def __init__(self, time_in_millis: int, position: Position) -> None:
        self.time_in_millis = time_in_millis
        predicted = PredictedPosition.predict_position(position, time_in_millis)
        self.latitude = predicted.latitude
        self.longitude = predicted.longitude
        self.speed = predicted.speed
        self.acceleration = predicted.acceleration
        self.heading = predicted.heading
        self.real_position = position

    @staticmethod
    def predict_position(position: Position, time_in_millis: int) -> Position:
        """
        Predict a position based on a real position.
        """
        easting, northing, z_number, z_letter = position.transform_to_utm()
        time_in_seconds = time_in_millis * 0.001
        predicted_easting = easting + position.speed * math.sin(position.heading/180*math.pi) * \
            time_in_seconds + 0.5 * position.acceleration * math.sin(position.heading/180*math.pi) * \
            time_in_seconds ** 2
        predicted_northing = northing + position.speed * math.cos(position.heading/180*math.pi) * \
            time_in_seconds + 0.5 * position.acceleration * math.cos(position.heading/180*math.pi) * \
            time_in_seconds ** 2
        lat, lon = utm.to_latlon(predicted_easting, predicted_northing, z_number, z_letter)
        return Position(time_in_millis, lat, lon, position.speed, position.acceleration, position.heading)

class Vehicle:
    """
    Class that represents a Vehicle.
    
    Attributes
    ----------
    current_position : PredictedPosition
        Current position of the vehicle.
    last_real_position : Position
        Last real position of the vehicle.
    last_predicted_position : list[PredictedPosition]
        Last predicted positions of the vehicle. [present -> past]
        [last 40]
    list_of_positions : list[Position]
        List of all positions of the vehicle. [present -> past]
        [last 5]
    """
    current_position: PredictedPosition
    last_real_position: Position
    last_predicted_positions: list[PredictedPosition]
    list_of_positions: list[Position]
    aoi_value: float
    
    def __init__(self, real_position : Position) -> None:
        self.current_position = PredictedPosition(0, real_position)
        self.last_real_position = real_position
        self.last_predicted_positions = []
        self.list_of_positions = []
        self.aoi_value = 0
    
    def update_position(self, new_position: Position) -> None:
        """
        Update the position of the vehicle.
        """
        print("Updating position")
        self.aoi_value = self.compute_peak_paoi(new_position)
        self.last_real_position = new_position
        self.list_of_positions.insert(0, new_position)
        if len(self.list_of_positions) > 5:
            self.list_of_positions.pop()
        self.last_predicted_positions.insert(0, self.current_position)
        if len(self.last_predicted_positions) > 40:
            self.last_predicted_positions.pop()
        self.current_position = PredictedPosition(0, new_position)
    
    def recompute_position(self, timestamp_in_millis: int) -> None:
        """
        Recompute the position of the vehicle.
        """
        time_in_millis = timestamp_in_millis-self.last_real_position.timestamp_in_millis
        self.last_predicted_positions.insert(0, self.current_position)
        if self.last_predicted_positions > 40:
            self.last_predicted_positions.pop()
        self.current_position = PredictedPosition(time_in_millis, self.last_real_position)
    
    def compute_peak_paoi(self, new_position : Position) -> float:
        """
        Compute if the vehicle is in the area of interest.
        
        Parameters
        ----------
        new_position : Position
            New position of the vehicle.
        
        Returns
        -------
        float
            Age of Information value.
        """
        time_in_millis = new_position.timestamp_in_millis - self.last_real_position.timestamp_in_millis
        predicted = PredictedPosition(time_in_millis, self.last_real_position)
        return new_position.distance(predicted)        

class TinyDigitalTwin:
    """
    Class that represents a tiny digital twin. That tracks vehicles positions.
    
    Attributes
    ----------
    vehicles : dict[str, Vehicle]
        Dictionary of vehicles. Key: vehicle_id, Value: Vehicle.
    
    """
    def __init__(self):
        self.vehicles = {}
    
    def clean_vehicles(self) -> None:
        """
        Clean vehicles that have not been updated in a while.
        """
        current_time_in_millis = time.time()*1000
        vehicles_to_remove = []
        for vehicle_id, vehicle in self.vehicles.items():
            if vehicle.last_real_position.timestamp_in_millis < current_time_in_millis-20000:
                vehicles_to_remove.append(vehicle_id)
        for vehicle_id in vehicles_to_remove:
            self.vehicles.pop(vehicle_id)
            
    
    def update_vehicle_position(self, vehicle_id: str, new_position: Position) -> None:
        if vehicle_id in self.vehicles:
            self.vehicles[vehicle_id].update_position(new_position)
        else:
            self.vehicles[vehicle_id] = Vehicle(new_position)
    
    def recompute_positions(self) -> None:
        self.clean_vehicles()
        timestamp_in_millis: int = time.time()*1000
        for _, vehicle in self.vehicles.items():
            vehicle.recompute_position(timestamp_in_millis)
    
    def get_average_peak_paoi(self) -> None:
        self.clean_vehicles()
        number_of_vehicles = len(self.vehicles)
        total_paoi = 0
        for _, vehicle in self.vehicles.items():
            total_paoi += vehicle.aoi_value
        if number_of_vehicles == 0:
            return 0
        return total_paoi/number_of_vehicles