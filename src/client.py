import socket
import json
from prometheus_aoi_exposer import PrometheusAoIExposer
from tiny_digital_twin import TinyDigitalTwin, Position
import threading
import argparse

class FreshnessClient:
    """
    This class connects to the FreshnessServer and gets the positions messages.
    
    Attributes
    ----------
    server_address : str
        Address of the FreshnessServer.
    server_port : int
        Port of the FreshnessServer.
    """
    def __init__(self, server_address: str, server_port: int):
        self.server_address = server_address
        self.server_port = server_port
        self.stop_event = threading.Event()
        self.tiny_digital_twin = TinyDigitalTwin()
        self.prometheus_exposer = PrometheusAoIExposer()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.thread = threading.Thread(target=self.receive_positions)
        self.thread.start()
    
    def process_receive_message(self, data: bytes):
        messages : list[str] = FreshnessClient.correct_message(data)
        for message in messages:
            self.process_message(message)
        
    def process_message(self, data: str):
        """
        Process the message received from the FreshnessServer.
        
        Parameters
        ----------
        data : bytes
            Message received from the FreshnessServer.
            
        The message is a JSON object with the following structure:
        {
            "timestamp": int,
            "vehicle_id": str,
            "latitude": float,
            "longitude": float,
            "speed": float,
            "heading": float
            "acceleration": float
        }
        """
        try:
            message = json.loads(data)
            position = Position(
                message["timestamp"],
                message["latitude"],
                message["longitude"],
                message["speed"],
                message["acceleration"],
                message["heading"]
            )
            if "vehicle_id" not in message:
                raise KeyError("vehicle_id")
            self.tiny_digital_twin.update_vehicle_position(message["vehicle_id"], position)
            self.prometheus_exposer.set_average_peak_paoi(self.tiny_digital_twin.get_average_peak_paoi())
            self.prometheus_exposer.set_number_of_vehicles(len(self.tiny_digital_twin.vehicles))
        except json.JSONDecodeError as e:
            print(f"Error decoding JSON: {e}")
            print(f"Data: {data}")
        except KeyError as e:
            print(f"Key not found: {e}")
            print(f"Data: {data}")
    
    @staticmethod
    def correct_message(data : bytes) -> list:
        to_return = []
        data = data.decode("utf-8")
        data = data.split("}{")
        for message in data:
            if len(message) > 10:
                n_message = message
                if message[0] != "{":
                    n_message = "{" + n_message
                if message[-1] != "}":
                    n_message = n_message + "}"
                to_return.append(n_message)
        return to_return
    
    def receive_positions(self):
        """
        Connect to the FreshnessServer.
        """
        self.socket.connect((self.server_address, self.server_port))
        while not self.stop_event.is_set():
            data = self.socket.recv(1024)
            if not data:
                break
            self.process_receive_message(data)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Freshness Client")
    parser.add_argument("--address", type=str, help="Address of the FreshnessServer")
    parser.add_argument("--port", type=int, help="Port of the FreshnessServer")
    args = parser.parse_args()

    client = FreshnessClient(args.address, args.port)
    try:
        client.thread.join()
    except KeyboardInterrupt:
        client.stop_event.set()
        client.thread.join()
    print("Closing Freshness Client...")