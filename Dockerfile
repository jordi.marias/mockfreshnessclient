FROM python:3.12.2-alpine3.19

# Set the working directory in the container
ADD . /app

WORKDIR /app

RUN pip install -r requirements.txt

# Command to run on container start
ENTRYPOINT [ "python" , "-u", "src/client.py"]
CMD [ "--address", "server", "--port", "9000" ]